'''
Created on 5/18/2018
by Nicholas Vercruysse
for Dynamo Technologies, LLC
for USDA Forest Service eMNEPA Project


This reads from an Apache access log and creates a
Pandas DataFrame from it, including the IP, Datetime, Request, and User-Agent

It is currently set up to filter the DataFrame by datetime and present aggregate
summaries based on IP and User-Agent. Modify as necessary for your use case

You will obviously need to change the file location at the top
'''

from datetime import datetime
import pandas as pd

IP_KEY = "IP Address"
DATE_KEY = "Datetime"
REQ_KEY = "Request"
AGENT_KEY = "Agent"
FILE_LOC = "C:\\Users\\Nicholas Vercruysse\\Documents\\Bugs\\access_log"
def get_x_date(request_message):
    start_range = input(request_message)
    date_time_start = ""
    try:
        date_time_start = datetime.strptime(start_range, '%d/%m/%Y:%H:%M:%S')
    except ValueError as e:
        date_time_start = get_x_date(request_message)
    return date_time_start



def main():
    print("All datetimes must be in current time in Virginia (EST or EDT, depending)")
    start_datetime_message = "Please enter the start of the datetime range in DD/MM/YYYY:24HH:mm:SS format\n"
    start_datetime = get_x_date(start_datetime_message)
    end_datetime_message = "Please enter the end of the datetime range in DD/MM/YYYY:24HH:mm:SS format\n"
    end_datetime = get_x_date(end_datetime_message)
    data = []
    with open(FILE_LOC) as log:

        lines = log.readlines()
        for line in lines:
            row = {}
            #Each line follow this pattern:
            #ip.add.re.sss - - [%d/%b/%Y:%H:%M:%S timezone] "GET/POST/PUT /path/to/requested/resource?params=values" \
            #HTTP_Response 0 more_numbers "-" "Agent"
            first_split = line.split('"')
            second_split = first_split[0].split(" ")
            row[IP_KEY] = second_split[0]
            date_time = datetime.strptime(second_split[3].replace("[", ""), '%d/%b/%Y:%H:%M:%S')
            row[DATE_KEY] = date_time
            row[REQ_KEY] = first_split[1]
            row[AGENT_KEY] = first_split[5]
            data.append(row)

    df = pd.DataFrame(data)
    df[DATE_KEY] = pd.to_datetime(df[DATE_KEY])
    relevant_df = df[(df[DATE_KEY] >= start_datetime) & (df[DATE_KEY] <= end_datetime)]
    print("grouped by IP:\n")
    print(relevant_df.groupby([IP_KEY]).agg(['count']))
    print("grouped by agent: \n")
    print(relevant_df.groupby([AGENT_KEY]).agg(['count']))


if __name__ == "__main__":
    main()