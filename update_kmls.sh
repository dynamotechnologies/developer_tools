#!/bin/bash

echo "setting up virtualenv...";
sudo /usr/bin/easy_install virtualenv;
virtualenv kml_env;
source kml_env/bin/activate;
echo "installing beautifulsoup...";
pip install beautifulsoup4;
echo "updating unit kmls...";
for unit_file in unit_*.kml; do python update_kml_url.py -f $unit_file; done;
deactivate;
rm -rf kml_env;
echo "done.";
