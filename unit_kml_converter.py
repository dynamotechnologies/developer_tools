import sys
import argparse
import re
from bs4 import BeautifulSoup as bs

'''
This tool updates a unit KML file. It will collect values specified in the ExtendedData tag for 
each project, and insert them into the CDATA section of a project-specific style tag, which will 
be used for an individual project Placemark. Finally, each ExtendedData tag will be removed, since 
these are not compatible with the ArcGIS Javascript API. 
'''

unit_kml_top_template = '''<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2"><Document><name>Projects for forest </name>'''

unit_kml_bottom_template = '''</Document></kml>'''

style_template = '''<Style id="red_dot"><BalloonStyle><text><![CDATA[<div style="margin:5px;"><div style="background-color:#613722;padding:5px;"><a href="$[url]" target="_blank" style="font-size:18px;font-weight:bold;color:white;text-decoration:none;">$[name]</a></div><div><span style="font-weight:bold;color:#613722">Status: </span>$[status] <span style="font-weight:bold;color:#613722">Analysis Type: </span>$[analysistype]<br/><span style="font-weight:bold;color:#613722">Purpose(s): </span>$[purposes]</div>]]></text></BalloonStyle><IconStyle><scale>1.5</scale><Icon><href>http://maps.google.com/mapfiles/ms/icons/red-dot.png</href></Icon></IconStyle></Style>'''

def main(kml_file):
    global unit_kml_top_template
    global unit_kml_bottom_template
    global style_template

    new_kml = unit_kml_top_template

    with open(kml_file, 'r') as in_file:
        kml = re.sub("[\t\n]", "", in_file.read())
        soup = bs(kml, 'xml')

        patterns = ["red_dot", "\$\[name\]", "\$\[url\]", "\$\[status\]", "\$\[analysistype\]", "\$\[purposes\]"]

        placemarks = soup.find_all('Placemark')
        for placemark in placemarks:
            try:
                id = "style_" + placemark.get('id')
                name = placemark.find('Data', {'name': "name"}).text.strip()
                url = placemark.find('Data', {'name': "url"}).text.strip()
                status = placemark.find('Data', {'name': "status"}).text.strip()
                analysistype = placemark.find('Data', {'name': "analysistype"}).text.strip()
                purposes = placemark.find('Data', {'name': "purposes"}).text.strip()
            except:
                print("conversion failed: " + kml_file)
                return

            replacements = [id, name, url, status, analysistype, purposes]

            new_style_tag = style_template
            for pattern, replacement in zip(patterns, replacements):
                new_style_tag = re.sub(pattern, replacement, new_style_tag)

            new_kml += new_style_tag

            project_placemark = re.sub("red_dot", id, str(placemark))
            project_placemark = re.sub("<ExtendedData>.*</ExtendedData>", "", project_placemark)

            new_kml += project_placemark

        new_kml += unit_kml_bottom_template

        with open(kml_file, 'w') as new_kml_file:
            new_kml_file.write(new_kml)
        new_kml_file.close()

if __name__ == "__main__":
    aparser = argparse.ArgumentParser(description='Convert Unit KML files to remove ExtendedData tags')
    aparser.add_argument('-f', '--kml_file', default=None, help='KML file to convert')

    args = aparser.parse_args()
    kml_file = args.kml_file
    main(kml_file)
