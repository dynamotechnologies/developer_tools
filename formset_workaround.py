import re

'''
# creates csv of dmd ids for query to add doc label
with open('1809_2278_35363.csv', 'r') as f:
    print(','.join(['"' + id.strip('\n') + '"' for id in f.readlines()]))
'''

#'''
# creates output of query for correct format for POST variable
# format for each letter: documents,_Public_Comment___LastName,DMDID|
with open('dmds.csv', 'r') as f:
    pstring = '|documents,_' + re.sub('-', '_', re.sub(' ', '_', re.sub('\"', '', '|documents,_'.join(f.read().split('\n')[1:-1]))))
    pstring = ','.join([s for s in pstring.split(',') if not (s[0] == '_' and s[:7] != '_Public')])
    p_split = pstring.split('|')

# replace #'s below with CARA #, Phase #, CARA #
downloadSpec = "CARA_1809_PHASE_2278_2|d,e,g,h|2278"

for doc in p_split[1:]:
    if len(doc.split(',')) < 3:
        p_split[p_split.index(doc)] = doc.split(',')[0] + ',_Public_Comment,' + doc.split(',')[1]

downloadSpec += '|'.join(p_split)
print(downloadSpec)
#'''
