import requests
import re
import xmltodict
import argparse

'''
This tool is used to publish OR unpublish documents en masse. It is run from the command line, 
with the argument -f denoting the csv file to be used, formatted as only dmd_id for each row. 
The user will be asked for authorization info to run the script, which utilizes 2 API calls for 
each document in order to retrieve the associated file and and to remove the document.
The tool is called by executing: python publish_docs.py -p <project_num> -f <csv_file> -a <action>
'''

broker_url = 'https://data.ecosystem-management.org/dmdpilot/broker/broker.php'
document_url = 'https://data.ecosystem-management.org/api/1_0/projects/nepa/'
headers = {'Content-Type': 'application/xml'}
xml = '''<methodCall>
    <methodName>publishFile</methodName>
    <params>
        <param>
            <value>
                <string>PALS_WWW_1</string>
            </value>
        </param>
        <param>
            <value>
                <string>dmd_id</string>
            </value>
        </param>
        <param>
            <value>
                <struct>
                    <member>
                        <name>targetdir</name>
                        <value>
                            <string/>
                        </value>
                    </member>
                    <member>
                        <name>targetname</name>
                        <value>
                            <string>file_name</string>
                        </value>
                    </member>unpublish_node
                </struct>
            </value>
        </param>
    </params>
</methodCall>'''
unpublish_xml = '''
                    <member>
                    	<name>action</name>
                    	<value>
                    		<string>rm</string>
                    	</value>
                    </member>'''


def main(project, csv_file, action):
    global broker_url
    global document_url
    global headers
    global xml
    global unpublish_xml

    print('Enter credentials to use API')
    auth = (input('Username: '), input('Password: '))

    if action == 'publish':
        xml = re.sub('unpublish_node', '', xml)
    elif action == 'unpublish':
        xml = re.sub('unpublish_node', unpublish_xml, xml)
    else:
        print('incorrect action specified, try again.')
        exit()

    with open(csv_file, 'r', encoding='utf8') as f:
        for row in f.readlines():
            doc_id = str(row).strip()
            # get file name for document from doc_id
            document_api_url = document_url + project + '/docs/' + doc_id
            doc_response = requests.get(document_api_url, auth=auth)
            if doc_response.status_code != 200:
                print('Error getting doc info from API for {}, continuing to next doc'.format(doc_id))
                continue
            else:
                doc_xml = doc_response.text
                doc_xml = xmltodict.parse(doc_xml)
                file_name = doc_xml['projectdocument']['wwwlink'].split('/')[-1]

            # remove file from UCM
            xml = re.sub('dmd_id', doc_id, xml)
            xml = re.sub('file_name', file_name, xml)
            broker_response = requests.post(broker_url, data=xml, headers=headers, auth=auth)
            if broker_response.status_code != 200:
                print('Unsuccessful {}ing of document '.format(action) + doc_id + ' from UCM')
                continue
            print('Successfully {}ed document '.format(action) + doc_id)


if __name__ == "__main__":
    aparser = argparse.ArgumentParser(description='unpublish documents')
    aparser.add_argument('-p', '--project', default=None, help='project number associated with docs')
    aparser.add_argument('-f', '--csv_file', default=None, help='file containing project_ids and dmd_ids')
    aparser.add_argument('-a', '--action', default='publish', choices={'publish', 'unpublish'},
                         help='specify action (publish/unpublish)')

    args = aparser.parse_args()
    project = args.project
    csv_file = args.csv_file
    action = args.action
    main(project, csv_file, action)
