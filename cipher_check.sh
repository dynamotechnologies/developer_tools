# usage:
# bash cipher_check.sh ip:port

# notes:
# you need cygwin to run it on Windows (Git Bash will work)
# you need openssl installed
# Also, this doesn't print yes and no, it prints NO and the entire cert chain info
# more than you'd want maybe, but it's useful info
SERVER=$1
DELAY=1
ciphers=$(openssl ciphers 'ALL:eNULL' | sed -e 's/:/ /g')
echo Obtaining cipher list from $(openssl version).

for cipher in ${ciphers[@]}
do
	echo -n Testing $cipher on $SERVER...
	result=$(echo -n | openssl s_client -cipher "$cipher" -connect $SERVER 2>&1)

	if [[ "$result" =~ ":error:" ]]; then
		error=$(echo -n $result | cut -d':' -f6)
		echo NO \($error\)
	else
		if [["$result" =~ "Cipher is ${cipher}" || "$result" =~ "Cipher    :" ]]; then
			echo YES
		else
			echo UNKNOWN RESPONSE
			echo $result
		fi
	fi
	sleep $DELAY
done
