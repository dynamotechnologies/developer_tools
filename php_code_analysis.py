import os
import re

func_dict = {}
repo_dir = 'C:\\Users\\Nicholas Vercruysse\\CodeCommit\\oregon\\downloader'
p = re.compile('.*\.php')
for root, subdirs, files in os.walk(repo_dir):
    for filename in files:
        m = p.match(filename)
        if m:
            with open(os.path.join(root, filename)) as php_code:
                lines = "\n".join(php_code.readlines())
                func_p = re.compile('function .*\(.*?\)')
                funcs_m = func_p.finditer(lines)
                for func in funcs_m:
                    #print(func.group())
                    func_str = func.group()
                    if func_str == 'function ()':
                        continue
                    left_paren = func_str.index('(')
                    #function is the same length, always, so index 8  = space, 9 = start of name
                    start = 9
                    func_name = func_str[start:left_paren]
                    if (len(func_name) == 0):
                        continue
                    func_params_p = re.compile("\((.*?)\)")
                    func_params_m = func_params_p.search(func_str)
                    func_params = func_params_m.group(1)
                    num_params = 0
                    if (len(func_params) > 0):
                        params = func_params.split(',')
                        #determine if any of them have an = after them; then they are optional and
                        #shouldn't count toward num_params
                        opts = 0
                        for param in params:
                            if (param.find('=') > 0):
                                opts += 1
                        num_params = len(func_params.split(',')) - opts
                    #print(num_params)
                    func_dict[func_name] = num_params

#Found all of the functions. Since functions can appear outside the file where they are defined
#Have to do that loop over again, this time checking just for the function name and the
#number of parameters to deteremine if they match
#for key, value in func_dict.items():
#    print(key + ": " + str(value) + "\n")
for root, subdirs, filenames in os.walk(repo_dir):
    for filename in filenames:
        m = p.match(filename)
        if m:
            with open(os.path.join(root, filename)) as php_code:
                lines = '\n'.join(php_code.readlines())
                for key, value in func_dict.items():
                    func_p = re.compile(key + '\((.*?)\)')
                    func_ms = func_p.finditer(lines)
                    for func_m in func_ms:
                        func_params = func_m.group(1)
                        #print(key + ": " + func_params + "; " + str(value))
                        num_params = 0
                        if len(func_params) > 0:
                            num_params = len(func_params.split(','))
                        if (num_params < value):
                            print(key + " (" + func_params + "); " + str(value) + " - " + os.path.join(root, filename))




