import urllib3
from time import sleep
from bs4 import BeautifulSoup

query_url = "http://ucm.ecosystem-management.org/web/idcplg?IdcService=WORK_IN_PROGRESS"
resubmit_url = "http://ucm.ecosystem-management.org/web/idcplg?IdcService=RESUBMIT_FOR_CONVERSION&dID="
headers = {"Authorization": "Basic c3lzYWRtaW46cDFjZ29yY2w=",
           "Cookie": "IntradocAuth=Internet; IdcLocale=English-US; IdcTimeZone=America/New_York; IntradocLoginState=1"}

http = urllib3.PoolManager()
r = http.request('GET', query_url, headers=headers)
html = BeautifulSoup(r.data.decode('utf-8'), 'html.parser')
# All documents are in a hidden input tag that has the attribute name="dID"
tags = html.find_all('input', {'name': 'dID'})
for tag in tags:
    sleep(0.1)
    dID = tag.attrs['value']
    resubmit_req = resubmit_url + dID
    req = http.request('GET', resubmit_req, headers=headers)
    print(req.status)




