import os
import fnmatch
path = 'C:\\Users\\Nicholas Vercruysse\\CodeCommit\\oregon\\DataMart'
import_dict = {}
for root, dirnames, filenames in os.walk(path):
    for filename in fnmatch.filter(filenames, '*.java'):
        full_filename = root + '\\' + filename
        with open(full_filename) as java_file:
            lines = java_file.readlines();
            imports = filter(lambda x: 'import' in x, lines)
            for java_import in imports:
                if java_import not in import_dict:
                    import_dict[java_import] = []

                import_dict[java_import].append(full_filename)



for key, value in import_dict.items():
    print(key[:-2] + ': ' + str(value))

