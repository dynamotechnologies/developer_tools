import sys
import argparse
import re
from bs4 import BeautifulSoup

'''
This tool is used to update the project links in pins for unit KML files to direct to the project page
instead of the forest-wide home page. The KML files are edited in place, and this script is run from the command
line. 
'''


def main(kml_file):
    with open(kml_file, 'r') as in_file:
        kml = re.sub("[\t\n]", "", in_file.read())

        soup = BeautifulSoup(kml, 'html.parser')

        base_url = 'https://www.fs.usda.gov/project/?project='

        proj_urls = []
        placemarks = soup.find_all('placemark')
        for placemark in placemarks:
            proj_urls.append(base_url + placemark['id'])

        for url in proj_urls:
            kml = re.sub('href=[\'"]?([^\'" >]+)', url, kml, count=1)

        kml = re.sub('a https:', 'a href=\"https:', kml)

    with open(kml_file, 'w') as new_kml:
        new_kml.write(kml)
        print('updated file: ', kml_file)


if __name__ == "__main__":
    aparser = argparse.ArgumentParser(description='Update Unit KML files to have Project page links')
    aparser.add_argument('-f', '--kml_file', default=None, help='Unit KML file to convert')

    args = aparser.parse_args()
    kml_file = args.kml_file
    main(kml_file)
