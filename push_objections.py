import pandas as pd
import numpy as np
import requests
from getpass import getpass
import argparse
import os

null = "nan"
user = None
pswd = None

def format_xml(row: pd.Series):
    xml_template = '''
        <?xml version="1.0" encoding="utf-8"?>
        <commentphase xmlns="http://www.fs.fed.us/nepa/schema" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
    	    <phaseid>PHASE</phaseid>
    	    <name>NAME</name>
    	    <startdate>START</startdate>
    	    <finishdate>FINISH</finishdate>
    	    <lettno>0</lettno>
    	    <lettuniq>0</lettuniq>
    	    <lettmst>0</lettmst>
    	    <lettform>0</lettform>
    	    <lettformplus>0</lettformplus>
    	    <lettformdupe>0</lettformdupe>
        </commentphase>
    '''
    xml_template = xml_template.replace(r"PHASE", str(row['PhaseId']))
    if str(row['Name']) != null:
        xml_template = xml_template.replace(r"NAME", str(row['Name']))
    else:
        xml_template = xml_template.replace(r"NAME", "")

    xml_template = xml_template.replace(r"START", str(row['CommentStart']))
    xml_template = xml_template.replace(r"FINISH", str(row['CommentEnd']))
    return xml_template

def specify_url(row):
    url = "https://data.ecosystem-management.org/api/1_0/caraprojects/PROJECT/comment/phases"
    return url.replace(r"PROJECT", str(row['ProjectId']))

def submit_request(row):
    print("submitting {} to {}".format(row['xml'], row['url']))
    # The below code would be uncommented when running the script
    # response = requests.post(row['url'], data=row['xml'], auth=(user, pswd))
    # return response.status_code

def main(pwd, file):
    global user, pswd
    missing = pd.read_csv(pwd + file)
    missing['xml'] = missing.apply(format_xml, axis=1)
    missing['url'] = missing.apply(specify_url, axis=1)
    if user is None or pswd is None:
        user = input("Username for DataMart: ")
        # In PyCharm, you will need to comment this part out for the script to work
        # getpass is buggy in Qt-based IDEs (so like, all of them). Works on command line
        pswd = getpass("Password for DataMart: ")
    missing['status'] = missing.apply(submit_request, axis=1)
    missing.to_csv(pwd + "Documents\\objection_submission_results.csv")


if __name__ == "__main__":
    aparser = argparse.ArgumentParser(description='Submit a request ')
    aparser.add_argument('-f', '--csv_file', default="active_objection_comment_periods-missing_datamart.csv",
                         help='Objection CSV File')
    aparser.add_argument('-d', '--dir', nargs='?', default=os.getcwd(),
                         help='Directory of the csv file')
    args = aparser.parse_args()
    pwd = args.dir
    file = args.csv_file
    main(pwd, file)



