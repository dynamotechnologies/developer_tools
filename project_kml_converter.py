import sys
import argparse
import re
from bs4 import BeautifulSoup as bs

'''
This tool updates a KML file, inserting the values specified in the ExtendedData tag into CDATA, and 
then removing the entire ExtendedData tag, which is not compatible with the ArcGIS Javascript API. 
'''


def main(kml_file):
    with open(kml_file, 'r') as in_file:
        kml = re.sub("[\t\n]", "", in_file.read())
        soup = bs(kml, 'xml')

    try:
        name = soup.find('Data', {'name': "name"}).text.strip()
        url = soup.find('Data', {'name': "url"}).text.strip()
        status = soup.find('Data', {'name': "status"}).text.strip()
        analysistype = soup.find('Data', {'name': "analysistype"}).text.strip()
        purposes = soup.find('Data', {'name': "purposes"}).text.strip()
    except:
        print("conversion failed: " + kml_file)
        return

    patterns = ["\$\[name\]", "\$\[url\]", "\$\[status\]", "\$\[analysistype\]",
                "\$\[purposes\]", "<ExtendedData>.*</ExtendedData>"]
    replacements = [name, url, status, analysistype, purposes, ""]

    for pattern, replacement in zip(patterns, replacements):
        kml = re.sub(pattern, replacement, kml)

    with open(kml_file, 'w') as new_kml:
        new_kml.write(kml)
    new_kml.close()

if __name__ == "__main__":
    aparser = argparse.ArgumentParser(description='Convert Project KML files to remove ExtendedData tags')
    aparser.add_argument('-f', '--kml_file', default=None, help='KML file to convert')

    args = aparser.parse_args()
    kml_file = args.kml_file
    main(kml_file)
