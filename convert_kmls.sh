#!/bin/bash

echo "setting up virtualenv..."
sudo /usr/bin/easy_install virtualenv;
virtualenv kml_env;
source kml_env/bin/activate;
echo "installing beautifulsoup..."
pip install beautifulsoup4;
pip install lxml;
echo "converting project kmls..."
for project_file in project_*.kml; do python project_kml_converter.py -f $project_file; done;
echo "converting unit kmls..."
for unit_file in unit_*.kml; do python unit_kml_converter.py -f $unit_file; done;
deactivate;
rm -rf kml_env;
echo "done."
