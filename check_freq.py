import os

folders = os.walk('OneDrive_1_3-11-2019')
parent_folder = next(folders)
time_dicts = []
for file in parent_folder[2]:
    time_dict = {}
    with open(os.path.join(parent_folder[0], file)) as log:
        lines = log.readlines()

    time_lines = [x for x in lines if 'INFO' in x]
    time_dict = {}
    for line in time_lines:
        time = line.split(' ')[0].split(',')[0]
        if time not in time_dict:
            time_dict[time] = 0
        time_dict[time] += 1
    time_dicts.append(time_dict)
with open('api-response.log') as log:
    lines = log.readlines()

time_lines = [x for x in lines if 'INFO' in x]
time_dict = {}
for line in time_lines:
    time = line.split(' ')[0].split(',')[0]
    if time not in time_dict:
        time_dict[time] = 0
    time_dict[time] += 1
time_dicts.append(time_dict)
for time_dict in time_dicts:
    avg_per_sec = sum(time_dict.values()) / len(time_dict.keys())
    max_per_sec = max(time_dict.values())
    min_per_sec = min(time_dict.values())
    print(avg_per_sec, max_per_sec, min_per_sec)

    high_freq = {k: v for k, v in time_dict.items() if v > 100}
    print(high_freq)
    print(len(high_freq))

