import mysql.connector
import random

odd_tables = {'ProjectAddInfoDocs': 'documentId',
              'ProjectAdditionalInfo': 'AddInfoId',
              'ProjectImplInfo': 'implId',
              'ProjectImplInfoDocs': 'ImplInfoDocId',
              'ProjectMeetingNoticeDocs': 'meetingNoticeDocId',
              'ProjectMeetingNotices': 'meetingId',
              'ProjectViewDocs': 'ProjectViewDocsId',
              'ProjectViews': 'ViewId',
              'UserSession': 'Token'}

def test_int_id_range(cursor, new_cursor, table_name, mind, maxd):
    id_range = maxd - mind + 1
    num_tests = min([id_range, 20])
    test_ids = []
    for i in range(0, num_tests):
        pr_id = random.randint(int(mind), int(maxd) + 1)
        test_ids.append(pr_id)
    test_ids.append(mind)
    test_ids.append(maxd)
    index = 'Id'
    passed = True
    if table_name in odd_tables:
        index = odd_tables[table_name]
    for id in test_ids:
        query = '''select * from {}
                   where {} = {}'''.format(table_name, index, str(id))
        cursor.execute(query)
        new_cursor.execute(query)
        result = cursor.fetchone()
        new_result = new_cursor.fetchone()
        for i in range(0, len(result)):
            if result[i] != new_result[i]:
                passed = False
                print("old data: {} \n vs. new data: {}".format(result[i], new_result[i]))

    if passed:
        return "{} passed random row checks".format(table_name)
    else:
        return "{} failed random row checks".format(table_name)

def test_string_id(cursor, new_cursor, table_name, mind, maxd):
    results = []
    index = 'Id'
    if table_name in odd_tables:
        index = odd_tables[table_name]
    query = '''select * from {}
               where {} = '{}' 
               or {} = '{}\''''.format(
        table_name,
        index, mind,
        index, maxd
    )

    print(query)
    cursor.execute(query)
    new_cursor.execute(query)
    results = cursor.fetchall()
    new_results = cursor.fetchall()
    passed = True
    if (len(results) != len(new_results)):
        return "{} failed begin + end row check".format(table_name)

    for i in range(0, len(results)):
        result = results[i]
        new_result = results[i]
        for j in range(0, len(result)):
            if result[j] != new_result[j]:
                passed = False

    if passed:
        return "{} passed begin + end row check".format(table_name)
    else:
        return "{} failed begin + end row check".format(table_name)


cur_prod_conn = mysql.connector.connect(user='read_data',
                                        password='r3adyToDUMPD4t4',
                                        host='dmd-datamart-prod.chpygqy9b8nk.us-gov-west-1.rds.amazonaws.com',
                                        database='datamart1')

new_prod_conn = mysql.connector.connect(user='dmd_datamart',
                                        password='6s4TUuQHVCH6!',
                                        host='prod-dmd.chpygqy9b8nk.us-gov-west-1.rds.amazonaws.com',
                                        database='datamart1')
cursor = cur_prod_conn.cursor(buffered=True)
query = '''SELECT table_name FROM information_schema.tables
           WHERE table_schema=%s'''

new_cursor = new_prod_conn.cursor(buffered=True)
cursor.execute(query, ['datamart1'])
tests_run = 0
tests_passed = 0
for table_name in cursor:
    inner_cursor = cur_prod_conn.cursor()
    count_query = 'select count(*) as count from {}'.format(table_name[0])
    inner_cursor.execute(count_query)
    count = inner_cursor.fetchone()
    new_inner_cursor = new_prod_conn.cursor()
    new_inner_cursor.execute(count_query)
    new_count = new_inner_cursor.fetchone()
    if (count[0] == new_count[0]):
        print("{} passed count check".format(table_name[0]))
    else:
        print("{} failed count check".format(table_name[0]))
    if count[0] == 0:
        continue
    inner_cursor.close()
    new_inner_cursor.close()
    inner_cursor = cur_prod_conn.cursor()
    new_inner_cursor = new_prod_conn.cursor()
    min_max_query = 'select min(Id) as mind, max(Id) as maxd from {}'.format(table_name[0])
    if table_name[0] in odd_tables:
        index = odd_tables[table_name[0]]
        min_max_query = '''select min({}) as mind, 
                           max({}) as maxd from {}'''.format(index, index, table_name[0])

    inner_cursor.execute(min_max_query)
    new_inner_cursor.execute(min_max_query)
    (mind, maxd) = inner_cursor.fetchone()
    (mindn, maxdn) = new_inner_cursor.fetchone()
    if (maxd == maxdn and mind == mindn):
        print("{} passed count check".format(table_name[0]))
    else:
        print("{} failed count check".format(table_name[0]))

    inner_cursor.close()
    new_inner_cursor.close()

    inner_cursor = cur_prod_conn.cursor()
    new_inner_cursor = new_prod_conn.cursor()
    results = []
    id_could_be_int = True
    try:
        test_int_conversion = int(mind)
        test_int_conversion = int(maxd)
    except ValueError as e:
        id_could_be_int = False

    if id_could_be_int:
        if (str(int(mind)) == str(mind) and str(int(maxd)) == str(maxd)):
            pass_fail = test_int_id_range(inner_cursor, new_inner_cursor, table_name[0], int(mind), int(maxd))
        else:
            pass_fail = test_string_id(inner_cursor, new_inner_cursor, table_name[0], mind, maxd)
    else:
        pass_fail = test_string_id(inner_cursor, new_inner_cursor, table_name[0], mind, maxd)

    print(pass_fail)
    inner_cursor.close()
    new_inner_cursor.close()

# cursor.close()
# query = 'select count(*) as count from Projects'
# cursor = cur_prod_conn.cursor()
# cursor.execute(query)
# count = cursor.fetchone()
# print("There are {} entries in the Projects table".format(count))
#
# cursor.close()
# query = '''select min(Id) as mind, max(Id) as maxd from Projects'''
# cursor = cur_prod_conn.cursor()
# cursor.execute(query)
# (mind, maxd) = cursor.fetchone()
# cursor.close()
#
# cursor = cur_prod_conn.cursor()
# print("The min id in Projects is {} and the max is {}".format(mind, maxd))
# for i in range(0, 10):
#     pr_id = random.randint(mind, maxd + 1)
#     query = '''select * from Projects
#                where Id = %s'''
#     cursor.execute(query, [pr_id])
#     result = cursor.fetchone()
#     print(result)

cursor.close()
new_cursor.close()

cur_prod_conn.close()
new_prod_conn.close()
