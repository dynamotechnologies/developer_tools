import pyodbc, csv

LETTERID_I = 0
AUTHORNAME_I = 1
ADDRESS1_I = 2
CITY_I = 3
STATEID_I = 4
ZIPPOSTAL_I = 5

CONN_STRING = ('DRIVER={SQL Server};'
              'SERVER=cara-test.chpygqy9b8nk.us-gov-west-1.rds.amazonaws.com;'
              'DATABASE=CARA;'              
              'UID=###;'
              'PWD=######')

def add_address_by_letterId(authorTable):

    conn = pyodbc.connect(CONN_STRING)
    cursor = conn.cursor()

    cursor.execute('CREATE TABLE #tempTable (LetterId int, Address1 varchar(200), '
                   'City varchar(100), StateId char(2), ZipPostal varchar(20))')

    x = 0    
    for author in authorTable:
        cursor.execute('INSERT INTO #tempTable (LetterId, Address1, '
                       'City, StateId, ZipPostal) VALUES (?,?,?,?,?)',
                       [author[LETTERID_I],author[ADDRESS1_I],author[CITY_I],
                       author[STATEID_I],author[ZIPPOSTAL_I]])
        x += 1

    cursor.execute('UPDATE Author '
                   'SET Author.Address1 = #tempTable.Address1, '
                   'Author.City = #tempTable.City, '
                   'Author.StateId = #tempTable.StateId, '
                   'Author.ZipPostal = #tempTable.ZipPostal '
                   'FROM Author '
                   'JOIN #tempTable ON Author.LetterId = #tempTable.LetterId')
  
    # cursor.execute('SELECT Author.* FROM Author '
                   # 'JOIN #tempTable ON Author.LetterId = #tempTable.LetterId')
    # for row in cursor.fetchall():
    #    print (row)
    
    conn.commit()   
    cursor.close()
    conn.close()
    

def main():

    # vertDelimFile = 'ColsTrio_LetterId-PhaseId-PII.out'
    vertDelimFile = 'test.out'
    authorTable = ''
    
    with open(vertDelimFile,'r') as dFile:
        reader = csv.reader(dFile, delimiter='|')
        authorTable = list(reader) 

    add_address_by_letterId(authorTable)

    
if __name__ == "__main__":
    main()    