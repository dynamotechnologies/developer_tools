import urllib3
import xml.etree.ElementTree as et
from xml.etree.ElementTree import Element
from pprint import pprint

QUERY_URL = 'https://api.govdelivery.com/api/account/USDAFS/topics.xml'
headers = {"Authorization": "Basic "}
DATAMART_URL = "https://data.ecosystem-management.org/api/1_0/"

def main():
    http = urllib3.PoolManager()
    r = http.request('GET', QUERY_URL, headers=headers)
    xml_root: Element = et.fromstring(r.data)
    topics = xml_root.findall('topic')
    codes = [topic.getchildren()[2].text for topic in topics]
    rel_codes_gen = filter(lambda x: 'NEPA_' in x, codes)
    rel_code_set = {rel_code for rel_code in rel_codes_gen}
    mlm_list_set = set()
    with open('mlm_lists.csv', 'r') as mlm_lists:
        mlm_list = mlm_lists.readline()
        mlm_list_set.add(mlm_list)
    only_in_gov = rel_code_set - mlm_list_set
    only_in_mlm = mlm_list_set - rel_code_set
    pprint(only_in_gov)
    pprint(only_in_mlm)


if __name__ == '__main__':
    main()