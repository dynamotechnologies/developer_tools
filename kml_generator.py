import sys
import urllib3
import re
import getpass
import base64
import argparse
import xml.etree.ElementTree as et
from xml.etree.ElementTree import Element


"""
This tool creates KML files when provided a list of ProjectIDs, Latitude, Longitude
"""

# Create KML templates to have the information inserted with format()
# {} are filled in with values provided to format() later

project_kml_top_template = '''
<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2">
	<Document>
		<name>Project {}</name>
'''
unit_kml_top_template = '''
<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2">
	<Document>
		<name>>Projects for forest {}</name>
'''
kml_top_template = '''
		<Style id="red_dot">
			<BalloonStyle>
				<text>
					<![CDATA[<div style="margin:5px;"><div style="background-color:#613722;padding:5px;"><a href="$[url]" target="_blank" style="font-size:18px;font-weight:bold;color:white;text-decoration:none;">$[name]</a></div><div><span style="font-weight:bold;color:#613722">Status: </span>$[status] <span style="font-weight:bold;color:#613722">Analysis Type: </span>$[analysistype]<br/><span style="font-weight:bold;color:#613722">Purpose(s): </span>$[purposes]</div>]]>
				</text>
			</BalloonStyle>
			<IconStyle>
				<scale>1.5</scale>
				<Icon>
					<href>http://maps.google.com/mapfiles/ms/icons/red-dot.png</href>
				</Icon>
			</IconStyle>
		</Style>
'''
project_kml_template = '''
		<Placemark id="{1}">
			<name>{0}</name>
			<styleUrl>#red_dot</styleUrl>
			<ExtendedData>
				<Data name="id">
					<value>{1}</value>
				</Data>
				<Data name="name">
					<value>{0}</value>
				</Data>
				<Data name="url">
					<value>{2}</value>
				</Data>
				<Data name="status">
					<value>{3}</value>
				</Data>
				<Data name="analysistype">
					<value>{4}</value>
				</Data>
				<Data name="purposes">
					<value>{5}</value>
				</Data>
			</ExtendedData>
			<description></description>
			<Point>
				<coordinates>{6},{7},0.000000</coordinates>
			</Point>
		</Placemark>
'''

end_tag = '''
    </Document>
</kml>
'''

# Dictionaries filling in the meaning of the results returned by DataMart API for:
# - Status of projects (line 81)
# - Analysis type of projects (line 89)
# - Purposes of projects (line 96)
# DataMart contains this information as well, but these dictionaries reduce the number of calls to the database
REF_STATUS_ID = {
    '1': 'Developing Proposal',
    '2': 'In Progress',
    '3': 'On Hold',
    '4': 'Cancelled',
    '5': 'Completed'
}

REF_ANALYSIS = {
    'CE': 'Decision Memo',
    'EA': 'Environmental Assessment',
    'EIS': 'Environmental Impact Statement',
    'NA': 'Unknown'
}

REF_PURPOSES = {
    'FC': 'Facility management',
    'FR': 'Research',
    'HF': 'Fuels management',
    'HR': 'Heritage resource management',
    'LM': 'Land ownership management',
    'LW': 'Land acquisition',
    'MG': 'Minerals and Geology',
    'NA': 'Unknown',
    'PN': 'Land management and planning',
    'RD': 'Road management',
    'RG': 'Grazing management',
    'RO': 'Regulations, Directives, Orders',
    'RU': 'Special area management',
    'RW': 'Recreation management',
    'SU': 'Special use management',
    'TM': 'Forest products',
    'VM': 'Vegetation management (other than forest products)',
    'WM': 'Watershed management',
    'WF': 'Wildlife, Fish, Rare plants'
}

project_outfile = 'kmls/project_{}.kml'
unit_outfile = 'kmls/unit_{}.kml'

def replace_special_char(string, patterns):
    for pattern in patterns:
        string = re.sub(pattern, "&amp;", string)
    return string


def main(forest_code=110803, forest_url='https://www.fs.usda.gov/conf',
         pid_file='./CONF_pids_latlongs.csv'):
    global project_kml_top_template
    global unit_kml_top_template
    global kml_top_template
    global project_kml_template
    global end_tag
    '''
    Main function, short script that creates the KML files for the projects listed in the pid_file
    The pid_file is a header-less CSV file in the format PublicId, Latitude, Longitude

    :param forest_code: int: The forest code for which the KMLs are being updated
    :param forest_url: str: The homepage URL for the forest
    :param pid_file: str: The file location for the csv/txt file that contains the project number, latitude
                          and longitude for each project for which a KML will be generated
    :return: None;  writes out the files
    '''
    headers = {}
    user = input('Username: ')
    password = getpass.getpass()
    dm_auth = 'Basic {}'.format(base64.b64encode(bytes(user + ':' + password, 'utf-8')).decode('utf-8'))
    headers['Authorization'] = dm_auth
    proj_info = []
    # open file with project ids, latitudes, and longitudes, and write them to an array
    try:
        with open(pid_file) as file:
            projinfo_lines = file.readlines()
            for projline in projinfo_lines:
                proj_info.append(projline.split(','))
    except Exception as e:
        print(e)
    http = urllib3.PoolManager()
    query_url = "https://data.ecosystem-management.org/api/1_0/projects/nepa/{}"
    proj_kmls = []
    pattern = re.compile(r'^[ \t]+|[ \t\r\n]+&')
    project_kml_top_template = re.sub(pattern, "", project_kml_top_template)
    unit_kml_top_template = re.sub(pattern, "", unit_kml_top_template)
    kml_top_template = re.sub(pattern, "", kml_top_template)
    project_kml_template = re.sub(pattern, "", project_kml_template)
    end_tag = re.sub(pattern, "", end_tag)

    amp_pattern = re.compile(r'&')
    lt_pattern = re.compile(r'<')
    gt_pattern = re.compile(r'>')
    patterns = [amp_pattern, lt_pattern, gt_pattern]
    forest_url = replace_special_char(forest_url, patterns)
    # for each of the rows of proj_info, GET request from DataMart, parse and process XML response,
    # generate a project kml and write it to a file, and generate a sub-kml to be inserted into the unit kml
    for proj in proj_info:
        proj_num = proj[0]
        r = http.request('GET', query_url.format(proj_num), headers=headers)
        try:
            xml_root: Element = et.fromstring(r.data)
        except Exception as e:
            # TODO: determine what to do with e. Don't interupt whole process, do save it out
            # so that users can do something with it. As script, this works, as web
            print('{} failed for the following reason: '.format(proj_num))
            print(e)
            continue
        # Need to get the following from the xml_root of the DataMart response:
        # -name
        # -status
        # -analysis type (i.e. Decision Memo, etc.)
        # -purposes (i.e. Recreation management, etc.)
        tag_base = '{{http://www.fs.fed.us/nepa/schema}}{}'
        tags = [['name'], ['statusid'], ['nepainfo', 'analysistypeid'], ['purposeids', 'purposeid']]
        vals = {}
        # finds the tag in the XML to get the information and place it in a dictionary to be used when formatting
        # the kml file. When there are multiple tags in the same array, the later tags are children of the previous tags
        for tag in tags:
            base_tag = xml_root
            for sub_tag in tag:
                tag_iter = base_tag.iter(tag_base.format(sub_tag))
                base_tag = next(tag_iter)
            if tag[-1] not in vals:
                vals[tag[-1]] = base_tag.text
            else:
                vals[tag[-1]] += ',{}'.format(base_tag.text)
        kml = project_kml_top_template.format(proj_num) + kml_top_template
        try:
            vals['name'] = replace_special_char(vals['name'], patterns)
            proj_kml = project_kml_template.format(vals['name'], proj_num, forest_url, REF_STATUS_ID[vals['statusid']],
                                                   REF_ANALYSIS[vals['analysistypeid']],
                                                   ' '.join([REF_PURPOSES[x] for x in vals['purposeid'].split(',')]),
                                                   float(proj[2]), float(proj[1]))
        except Exception as e:
            # TODO: same thing as other exception
            print('{} failed for the following reason: '.format(proj_num))
            print(e)
            continue
        proj_kmls.append(proj_kml)
        kml += proj_kml
        kml += end_tag
        try:
            with open(project_outfile.format(proj_num), 'w') as file:
                file.write(kml)
        except Exception as e:
            #TODO: same
            print('{} failed for the following reason: '.format(proj_num))
            print(e)
            continue

    unit_kml = unit_kml_top_template.format(forest_code) + kml_top_template
    unit_kml += ''.join(proj_kmls)
    unit_kml += end_tag
    try:
        with open(unit_outfile.format(forest_code), 'w') as file:
            file.write(unit_kml)
    except Exception as e:
        print('{} failed for the following reason: '.format(forest_code))
        print(e)



if __name__ == "__main__":
    aparser = argparse.ArgumentParser(description='Create KML files')
    aparser.add_argument('-f', '--forest_code', default=None, help='code for the forest in which the projects reside')
    aparser.add_argument('-u', '--forest_url', default=None,
                         help='home page for the forest, starts with https://www.fs.usda.gov/ in most cases')
    aparser.add_argument('-p', '--pid_file', default=None, help='file location of the csv that contains the '
                                                                'project numbers, latitudes, and longitudes of '
                                                                'the projects for which KMLs will be made')
    args = aparser.parse_args()
    forest_code = args.forest_code
    forest_url = args.forest_url
    pid_file = args.pid_file
    main(forest_code, forest_url, pid_file)
