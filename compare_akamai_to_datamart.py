import requests

path = 'C:\\Users\\Nicholas Vercruysse\\Documents\\Bugs\\'
akamai_doc = 'nepa-file-list-08072018.txt' #tab delimited
datamart_doc = 'published_documents_in_datamart.csv'

def extract_document_ids(filepath, delimiter=',', doc_id_idx = 1):
    docs = []
    with open(filepath) as file:
        lines = file.readlines()
        for line in lines:
            vals = line.split(delimiter)
            doc_id = vals[doc_id_idx]
            docs.append(doc_id)

    return docs


def compare_ordered_lists(list_a, list_b):
    #read as in just a; and in just b
    in_a = []
    in_b = []

    i = 0
    j = 0
    n = len(list_a)
    m = len(list_b)
    while i < n and j < m:
        item_a = list_a[i]
        item_b = list_b[j]
        if (item_a < item_b):
            in_a.append(item_a)
            i += 1
            continue
        elif (item_b < item_a):
            in_b.append(item_b)
            j += 1
            continue
        else:
            i += 1
            j += 1
    while i < n:
        item_a = list_a[i]
        in_a.append(item_a)
        i += 1
    while j < m:
        item_b = list_b[j]
        in_b.append(item_b)
        j += 1

    return in_a, in_b


def print_dates_for_docs(filepath, checks, delimiter=',', doc_idx=1, date_idx=2):
    with open(filepath) as file:
        lines = file.readlines()
        for line in lines:
            vals = line.split(delimiter)
            doc_id = vals[doc_idx]
            if doc_id in checks:
                print(vals[date_idx])


def publish_to_service_broker(doc_id):
    prefix = 'FSPLT'
    pdf_suffix = '.pdf'
    if not doc_id.startswith(prefix):
        if prefix not in doc_id:
            raise ValueError('Doc ID must contain the string "FSPLT"')
        else:
            start_pref = doc_id.find(prefix)
            doc_id = doc_id[start_pref:]
    if pdf_suffix in doc_id:
        start_suffix = doc_id.find(pdf_suffix)
        doc_id = doc_id[0:start_suffix]
    headers = {'Content-Type': 'application/xml',
               'Authorization': 'Basic YnIwazNyOnMzcnYxYzM='}
    xml = """<?xml version="1.0" encoding="iso-8859-1"?>
               <methodCall>
                 <methodName>publishFile</methodName>
                 <params>
                   <param>
                     <value>
                       <string>PALS_WWW_1</string>
                     </value>
                   </param>
                   <param>
                     <value>
                       <string>""" + doc_id + """</string>
                     </value>
                   </param>
                   <param>
                     <value>
                       <struct>
                         <member>
                           <name>targetdir</name>
                           <value><string/></value>
                         </member>
                         <member>
                           <name>targetname</name>
                           <value><string/></value>
                         </member>
                         <member>
                           <name>fileType</name>
                           <value>
                             <string>PDF</string>
                           </value>
                         </member>
                       </struct>
                     </value>
                   </param>
                 </params>
               </methodCall>"""

    return requests.post('https://data.ecosystem-management.org/dmdpilot/broker/broker.php', data=xml, headers=headers).text

def publish_docs(docs):
    SUCCESS = '<boolean>1</boolean>'
    failed = []
    for doc in docs:
        response = publish_to_service_broker(doc)
        if SUCCESS not in response:
            failed.append(doc)

    return failed


data_docs = extract_document_ids(path + datamart_doc)
akamai_docs = extract_document_ids(path + akamai_doc, delimiter='\t')

data_docs.sort()
akamai_docs.sort()
print(len(data_docs))
print(len(akamai_docs))
print(data_docs[:5])
print(akamai_docs[:5])
in_datamart, in_akamai= compare_ordered_lists(data_docs, akamai_docs)
print(in_datamart)
print_dates_for_docs(path + akamai_doc, in_datamart, delimiter='\t')
print(len(in_datamart))
with open(path + "only_in_datamart.csv", 'w') as file:
    for doc in in_datamart:
        file.write(doc)
        file.write("\n")

failed_publishes = publish_docs(in_datamart)
with open(path + "failed_to_publish.csv", 'w') as file:
    for doc in failed_publishes:
        file.write(doc)
        file.write('\n')



